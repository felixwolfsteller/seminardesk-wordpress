<?php
/**
 * @package SeminardeskPlugin
 */

namespace Inc\Base;

use Inc\Utils\AdminUtils as Utils;
use Inc\Utils\AdminUtils;

/**
 * Define sub-routines to uninstall SeminarDesk plugin
 */
class Uninstall
{
    /**
     * code that runs during plugin uninstall
     *
     * @return void
     */
    public function uninstall() 
    {
        // Clear all SeminarDesk entries form database
        $delete_all = Utils::get_option_or_default(SD_OPTION['delete'], false, false);
        if ( $delete_all == true ) {
            AdminUtils::delete_all_sd_objects();
            AdminUtils::delete_all_sd_options();
        }
        flush_rewrite_rules();
    }
}