<?php 
/**
 * @package SeminardeskPlugin
 */
namespace Inc\Callbacks;

/**
 * Callbacks require once content for admin pages
 */
class AdminCallbacks
{
	/**
     * Call the general admin page for SeminarDesk.
     *
     * @return void
     */
	public function adminGeneral()
	{
		return require_once( SD_ENV['path'] . '/admin/admin.php' );
	}
}