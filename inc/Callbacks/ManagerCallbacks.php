<?php 
/**
 * @package SeminardeskPlugin
 */
namespace Inc\Callbacks;

use Inc\Controllers\CptController;
use Inc\Controllers\TaxonomyController;
use Inc\Utils\AdminUtils;

class ManagerCallbacks
{
	public function text_field( $args )
	{
		$name = $args['option']  . '[' . $args['key'] . ']';
		$value = AdminUtils::get_option_or_default( $args['option'], '',  $args['key']);
		echo '<input type="text" class="' . $args['class'] . '" name="' . $name . '" value="' . $value . '" placeholder="' . $args['placeholder'] . '">';
	}

	public function slug_field( $args )
	{
		$this->text_field( $args );
		
		// add view link for slugs
		switch ( $args['type'] ){
			case 'cpt':
				$view_link = get_post_type_archive_link( $args['name'] );
				break;
			case 'txn':
				// $view_link = null;
				break;
			case 'term':
				$view_link = get_term_link( get_term_by('name', $args['name'], $args['taxonomy'] ) ) ;
				break;
			default:
				// $view_link = null;

		}
		if ( isset( $view_link ) ){
			$view_title = 'URL for this slug: ' . $view_link;
			echo '<a style="margin: 10px;" href="' . $view_link . '"  title="' . $view_title . '">view</a>';
		}
	}

	public function checkbox_field( $args )
	{
		$checkbox = get_option( $args['option'] );
		echo '<input type="checkbox" name="' . $args['option'] . '" value="1" class="' . $args['class'] . '" ' . ($checkbox ? 'checked' : '') . '>';
	}

	/**
	 * Sanitize array of slugs
	 * 
	 * @param array $inputs 
	 * @return array 
	 */
	public function sanitize_slugs( array $inputs )
	{
		$inputs_sanitized = array();
		foreach ( $inputs as $input => $input_value ){
			$input_value = sanitize_title( $input_value );
			// $input_value = sanitize_text_field( $input_value );
			$inputs_sanitized[$input] = $input_value;
		}
		return $inputs_sanitized;
	}

	/**
	 * create CPTs and TXNs with new slug and rewrite rules 
	 * 
	 * @param mixed $value_old 
	 * @param mixed $value_new 
	 * @return void 
	 */
	public function rewrite_slugs( $value_old, $value_new )
	{			
		
		// remove rewrite rule for old slug sd_txn_dates
		global $wp_rewrite;
		if ( $value_old['sd_slug_txn_dates'] === '' ){
			$rule = '^' . SD_TXN['sd_txn_dates']['slug_default'] . '/?$';
			// unset($wp_rewrite->extra_rules_top['^schedule/?$']);
			unset($wp_rewrite->extra_rules_top[$rule]);
		} else {
			$rule = '^' .  $value_old['sd_slug_txn_dates'] . '/?$';
			unset($wp_rewrite->extra_rules_top[$rule]);
		}

		$cpt_ctrl = new CptController();
		$cpt_ctrl->create_cpts();

		$txn_ctrl = new TaxonomyController();
		$txn_ctrl->create_taxonomies();
		$txn_ctrl->update_terms_slug();
		$txn_ctrl->custom_rewrite_rules();

		flush_rewrite_rules();
	}

    public function sanitize_checkbox( $input )
	{
		// return filter_var($input, FILTER_SANITIZE_NUMBER_INT);
		return ( isset($input) ? true : false );
	}

	public function admin_section_slugs()
	{
		_e('Customize the slugs of this plugin. Use with caution, might have unintended effects on a deployed WordPress instance.', 'seminardesk');
	}

	public function admin_section_debug()
	{
		_e('Manage the settings for development.', 'seminardesk');
	
	
	}
	public function admin_section_uninstall()
	{
		_e('Manage the settings for WP uninstaller of the SeminarDesk plugin.', 'seminardesk');
	
	}
}