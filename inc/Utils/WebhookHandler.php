<?php 
/**
 * @package SeminardeskPlugin
 */
namespace Inc\Utils;

use WP_Error;
use WP_REST_Response;
use Inc\Utils\TemplateUtils;
use Inc\Utils\AdminUtils;
use Inc\Utils\WebhookUtils as Utils;

/**
 * Handler for webhook actions
 * !serialized meta data (custom field sd_data) cannot and should not be queried!
 * https://wordpress.stackexchange.com/questions/16709/meta-query-with-meta-values-as-serialize-arrays
 */
class WebhookHandler
{
    /**
     * Process request and its notifications
     * 
     * Note: notification order in webhoock batch... labelGroup.create, facilitator.create, event.create, eventDate.create
     * 
     * @param array $request_json 
     * @return WP_REST_Response 
     */
    public function batch_request( $request_json )
    {
        $response_notifications = array();
        $notifications = $request_json['notifications'];     
        foreach ( $notifications as $notification ){
            switch ( $notification['action'] ){
                case 'event.create':
                    $response = $this->create_event($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'event.update':
                    $response = $this->update_event($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'event.delete':
                    $response = $this->delete_event($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'eventDate.create':
                    $response = $this->create_event_date($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'eventDate.update':
                    $response = $this->update_event_date($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'eventDate.delete':
                    $response = $this->delete_event_date($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'facilitator.create':
                    $response =$this->create_facilitator($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'facilitator.update':
                    $response = $this->update_facilitator($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'facilitator.delete':
                    $response = $this->delete_facilitator($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'labelGroup.create':
                    $response =$this->create_label($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'labelGroup.update':
                    $response = $this->update_label($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'labelGroup.delete':
                    $response = $this->delete_label($notification);
                    array_push( $response_notifications, $response );
                    break;
                case 'all.delete':
                    $response = $this->delete_all($notification);
                    array_push( $response_notifications, $response );
                    break;
                default:
                    $response = array(
                        'status'    => 400,
                        'message'   => 'action ' . $notification['action'] . ' not supported',
                        'action'    => $notification['action'],
                        'id'        => $notification['payload']['id'],
                    );
                    array_push( $response_notifications, $response );
            }
        }

        return new WP_REST_Response( [
            'message'       => 'webhook response',
            'requestId'     => $request_json['id'],
            'attempt'       => $request_json['attempt'],
            'notifications' => $response_notifications,
        ], 200);
    }


    /**
     * Create or update event via webhook from SeminarDesk
     *
     * @param array $notification 
     * @return WP_REST_Response|WP_Error
     */
    public function put_event( $notification )
    {
        $payload = (array)$notification['payload']; // payload of the request in JSON
        $sd_webhook = $notification;
        unset($sd_webhook['payload']);
        
        
        // checks if event_id exists and sets corresponding post_id
        $query = Utils::get_query_by_meta( 'sd_cpt_event', 'sd_event_id', $payload['id'] );
        $event_post_id = $query->post->ID ?? 0;
        $event_count = $query->post_count;
        $event_title = TemplateUtils::get_value_by_language( $payload['title'] );
        $txn_input = array();
        
        /**
         * Event Settings
         */
        // set post_status private on event setting 'detailpageAvailable'
        $detailpageAvailable = $payload['detailpageAvailable'] ?? true;
        if (  $detailpageAvailable === false){
            $post_status = 'private'; // not visible to users who are not logged in. Note: Append string 'Private: ' in front of the post title
            // $post_status = 'pending'; // post is pending review, but is visible for everyone
        } else {
            $post_status = 'publish'; // viewable by everyone
        }
        // update event setting previewAvailable for all corresponding event dates, if they already exist
        $post_previewAvailable = $query->post->sd_data['previewAvailable'] ?? null;
        $payload_previewAvailable = $payload['previewAvailable'] ?? true;
        if ( !empty($event_post_id) && $post_previewAvailable !== $payload_previewAvailable )
        {
            // query corresponding event dates
            $query_dates = Utils::get_query_by_meta( 'sd_cpt_date', 'sd_event_id', $query->post->sd_event_id );
            foreach ( $query_dates->posts as $post_date ){
                $post_date->preview_available = $payload_previewAvailable;
                update_post_meta( $post_date->ID, 'preview_available', $post_date->preview_available );
                $test = $query->post->sd_event_id;
            }
        }

        /**
         * sd_txn_facilitators
         */
        $facilitators = $payload['facilitators'] ?? null;
        if ( !empty($facilitators) ){
            foreach ( $facilitators as $facilitator ){
                Utils::add_term_tax_input( $txn_input, $facilitator['id'], 'sd_txn_facilitators' );
            }
        }

        /**
         * sd_txn_labels
         */
        $labels = $payload['labels'] ?? null;
        if ( !empty( $labels ) ){
            foreach( $labels as $label ){
                $label_term_name = 'l_id_' . $label['id'];
                Utils::add_term_tax_input( $txn_input, $label_term_name, 'sd_txn_labels' );
            }
        }
        
        /**
         * sd_cpt_events
         */
        // define metadata of the event
        $meta_input = array(
            'sd_event_id'  => $payload['id'],
            'sd_data'      => $payload,
            'sd_webhook'    => $sd_webhook,
        );
        // set attributes of the new event
        $event_slug = Utils::unique_post_slug( $event_title, $event_post_id );
        $event_attr = array(
            'post_type'     => 'sd_cpt_event',
            'post_title'    => $event_title,
            'post_name'     => $event_slug,
            'post_author'   => get_current_user_id(),
            // 'post_status'   => 'publish',
            'post_status'   => $post_status,
            'meta_input'    => $meta_input,
            'tax_input'     => $txn_input,
        );
        
        // create new post or update post with existing post id
        if ( !empty($event_post_id) ) {
            $event_attr['ID'] = $event_post_id;
            $message = 'Event updated';
            $event_post_id = wp_update_post( wp_slash($event_attr), true );

        } else {
            $message = 'Event created';
            $event_post_id = wp_insert_post(wp_slash($event_attr), true);
        }

        // return error if $post_id is of type WP_Error
        if (is_wp_error($event_post_id)){
            return $event_post_id;
        }

        return array(
            'status'        => 200,
            'message'       => $message,
            'action'        => $notification['action'],
            'eventId'       => $payload['id'],
            'eventWpId'     => $event_post_id,
            'eventWpCount'  => $event_count,
        );
    }

    /**
     * Create event via webhook from SeminarDesk utilizing put_event
     * Incase event id already exists, it will update existing event
     *
     * @param array $notification 
     * @return WP_REST_Response|WP_Error
     */
    public function create_event($notification)
    {
       return $this->put_event($notification);
    }

    /**
     * Update event via webhook from SeminarDesk utilizing put_event
     * Incase event id doesn't exists, it will create the event
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function update_event($notification)
    {
        return $this->put_event($notification);
    }

    /**
     * Delete event via event id
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function delete_event($notification) 
    {
        $payload = (array)$notification['payload'];

        $event_deleted = Utils::delete_post_by_meta( 'sd_cpt_event', 'sd_event_id', $payload['id'] );
        
        if ( empty($event_deleted) ){
            return array(
                'status'        => 404,
                'message'       => 'Nothing to delete. Event ID ' . $payload['id'] . ' does not exists',
                'action'        => $notification['action'],
                'eventId'   => $payload['id'],
            );
        }

        return array(
            'status'        => 200,
            'message'       => 'Event deleted',
            'action'        => $notification['action'],
            'eventId'       => $payload['id'],
            'eventWpId'     => $event_deleted->ID,
        );
    }

    /**
     * Create or update event date via webhook from SeminarDesk
     *
     * @param array $notification 
     * @return WP_REST_Response|WP_Error
     */
    public function put_event_date( $notification )
    {
        $payload = (array)$notification['payload'];
        $sd_webhook = $notification;
        unset($sd_webhook['payload']);

        // check if with event date associated event exists and get its WordPress ID
        $event_query = Utils::get_query_by_meta( 'sd_cpt_event', 'sd_event_id', $payload['eventId']);
        $event_wp_id = $event_query->post->ID ?? 0;
        $event_preview = $event_query->post->sd_data['previewAvailable'] ?? false;

        if (!isset($event_wp_id)){
            return array(
                'status'        => 404,
                'message'       => 'Event date not created. Associated event with the ID ' . $payload['eventId'] . ' does not exist',
                'action'        => $notification['action'],
                'eventDateId'   => $payload['id'],
                'eventId'       => $payload['eventId'],
            );
        }
        $event_count = $event_query->post_count;

        // check if event date exists and sets corresponding date_post_id
        $date_query = Utils::get_query_by_meta( 'sd_cpt_date', 'sd_date_id', $payload['id']);
        $date_post_id = $date_query->post->ID ?? 0;
        $date_count = $date_query->post_count;
        $date_title = TemplateUtils::get_value_by_language( $payload['title'] );

        /**
         * sd_txn_dates
         */
        $year = wp_date('Y', $payload['beginDate']/1000);
        $month = wp_date('m', $payload['beginDate']/1000);
        // get term ID for date year and create if term doesn't exist incl. months as its children terms
        $term_year = term_exists($year, 'sd_txn_dates'); 
        if (!isset($term_year)){
            $term_year = wp_insert_term($year, 'sd_txn_dates', array(
                'description' => __('Dates of', 'seminardesk') . ' ' . $year,
                'slug' => $year,
            ));
            for ($m = 1; $m <= 12; $m++){
                $m_padded = sprintf('%02s', $m);
                wp_insert_term($m_padded . '/' . $year, 'sd_txn_dates', array(
                    // 'alias_of'      => $year,
                    'description'   => __('Dates of', 'seminardesk') . ' ' . $m_padded . '/' . $year,
                    'parent'        => $term_year['term_id'],
                    'slug'          => $year . '-' . $m_padded,
                ));
            }
        }
        // add txn_dates terms (year and months) to tax_input array
        $term_month = term_exists($year . '-' . $month, 'sd_txn_dates');
        $txn_dates_terms = array(
            $term_year['term_id'], 
            $term_month['term_id'],
        );
        $txn_input = array(
            'sd_txn_dates' => $txn_dates_terms,
        );

        /**
         * sd_txn_facilitators
         */
        // get facilitator ids from event
        $event_post = get_post( $event_wp_id );
        $facilitators = $event_post->sd_data['facilitators'] ?? null;
        if ( !empty($facilitators) ){
            foreach ( $facilitators as $facilitator ){
                $term_event_name = $payload['eventId'] . '_' . $facilitator['id'];
                Utils::add_term_tax_input( $txn_input, $term_event_name, 'sd_txn_facilitators' );
            }
        }

        /**
         * sd_txn_labels
         */
        // define array of taxonomy terms keyed by sd_txn_labels for the sd_cpt_date
        $labels = $payload['labels'] ?? null;
        if ( !empty( $labels ) ){
            foreach( $labels as $label ){
                $label_term_name = 'l_id_' . $label['id'];
                Utils::add_term_tax_input( $txn_input, $label_term_name, 'sd_txn_labels' );
            }
        }
        
        $meta_input = array(
            'sd_date_id'        => $payload['id'],
            'sd_date_begin'     => $payload['beginDate'],
            'sd_date_end'       => $payload['endDate'],
            'sd_event_id'       => $payload['eventId'],
            'wp_event_id'       => $event_wp_id,
            'preview_available' => $event_preview ?? false,
            'sd_data'           => $payload,
            'sd_webhook'        => $sd_webhook,
        );
        $date_slug = Utils::unique_post_slug( $date_title, $date_post_id );
        $date_attr = array(
            'post_type'     => 'sd_cpt_date',
            'post_title'    => $date_title,
            'post_name'     => $date_slug,
            'post_author'   => get_current_user_id(),
            'post_status'   => 'publish',
            'meta_input'    => $meta_input,
            'tax_input'     => $txn_input,
        );
        
        // create new post or update post with existing post id
        if ( !empty($date_post_id) ) {
            $date_attr['ID'] = $date_post_id;
            $message = 'Event Date updated';
            $date_post_id = wp_update_post( wp_slash($date_attr), true );
        } else {
            $message = 'Event Date created';
            $date_post_id = wp_insert_post( wp_slash($date_attr), true );
        }
        
        // return error if $date_post_id is of type WP_Error
        if (is_wp_error($date_post_id)){
            return $date_post_id;
        }

        return array(
            'status'            => 200,
            'message'           => $message,
            'action'            => $notification['action'],
            'eventDateId'       => $payload['id'],
            'eventDateWpId'     => $date_post_id,
            'EventDateWpCount'  => $date_count,
            'eventId'           => $payload['eventId'],
            'eventWpId'         => $event_wp_id,
            'eventWpCount'      => $event_count,
        );
    }

    /**
     * Create event date via webhook from SeminarDesk utilizing put_event_date
     * Incase event date id already exists, it will update existing event date
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function create_event_date($notification)
    {   
        return $this->put_event_date( $notification );
    }

    /**
     * Update event date via webhook from SeminarDesk utilizing put_event_date
     * Incase event date id doesn't exists, it will create the event date
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function update_event_date($notification)
    {   
        return $this->put_event_date( $notification );
    }

    /**
     * Delete event date via webhook from SeminarDesk
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function delete_event_date($notification)
    {   
        $payload = (array)$notification['payload'];

        $date_deleted = Utils::delete_post_by_meta('sd_cpt_date', 'sd_date_id', $payload['id']);

        if ( empty($date_deleted) ){
            return array(
                'status'        => 404,
                'message'       => 'Nothing to delete. Event date ID ' . $payload['id'] . ' does not exists',
                'action'        => $notification['action'],
                'eventDateId'   => $payload['id'],
            );
        }
        
        return array(
            'status'        => 200,
            'message'       => 'Event date deleted',
            'action'        => $notification['action'],
            'eventDateId'   => $payload['id'],
            'eventDateWpId' => $date_deleted->ID,
        );
    }
        
    /**
     * Create or update facilitator via webhook from SeminarDesk
     *
     * @param array $notification 
     * @return WP_REST_Response|WP_Error
     */
    public function put_facilitator( $notification )
    {
        $payload = (array)$notification['payload'];
        $sd_webhook = $notification;
        unset($sd_webhook['payload']);

        /**
         * sd_cpt_facilitators
         */
        $query = Utils::get_query_by_meta( 'sd_cpt_facilitator', 'sd_facilitator_id', $payload['id'] );
        $facilitator_post_id = $query->post->ID ?? 0;
        
        /**
         * sd_txn_labels
         */
        $txn_input = array();
        $labels = $payload['labels'] ?? null;
        if ( !empty( $labels ) ){
            foreach( $labels as $label ){
                $label_term_name = 'l_id_' . $label['id'];
                Utils::add_term_tax_input( $txn_input, $label_term_name, 'sd_txn_labels' );
            }
        }
        // define metadata of the new sd_cpt_facilitator
        $meta_input = array(
            'sd_facilitator_id' => $payload['id'],
            'sd_last_name'      => $payload['lastName'],
            'sd_data'           => $payload,
            'sd_webhook'        => $sd_webhook,
        );
        // define attributes of the new facilitator using $payload of the 
        $slug = Utils::unique_post_slug( $payload['name'], $facilitator_post_id );
        $facilitator_attr = array(
            'post_type'         => 'sd_cpt_facilitator',
            'post_title'        => $payload['name'],
            'post_name'         => $slug,
            'post_author'       => get_current_user_id(),
            'post_status'       => 'publish',
            'meta_input'        => $meta_input,
            'tax_input'         => $txn_input,
        );
        // create new post or update post with existing post id
        if ( !empty($facilitator_post_id) ) {
            $facilitator_attr['ID'] = $facilitator_post_id;
            $message = 'Facilitator updated';
            $facilitator_post_id = wp_update_post( wp_slash($facilitator_attr), true );
        } else {
            $message = 'Facilitator created';
            $facilitator_post_id = wp_insert_post(wp_slash($facilitator_attr), true);
        }
        // return error if $post_id is of type WP_Error
        if (is_wp_error($facilitator_post_id)){
            return $facilitator_post_id;
        }

        /**
         * sd_txn_facilitators
         */
        // insert/update term for sd_cpt_facilitator, is parent of event term
        // set ID as name to be queryable
        $term_ids = Utils::set_term( 'sd_txn_facilitators', $payload['id'], $payload['name'], $slug );
        // return error if $term_ids is of type WP_Error
        if (is_wp_error($term_ids)){
            return $term_ids;
        }

        return array(
            'status'            => 200,
            'message'           => $message,
            'action'            => $notification['action'],
            'facilitatorId'     => $payload['id'],
            'facilitatorWpId'   => $facilitator_post_id,
        );
    }

    /**
     * Create facilitator via webhook from SeminarDesk
     * Incase facilitator id already exists, it will update existing facilitator
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function create_facilitator($notification)
    {
        return $this->put_facilitator( $notification );
    }

    /**
     * Update facilitator via webhook from SeminarDesk
     * Incase facilitator id doesn't exists, it will create the facilitator
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function update_facilitator($notification)
    {
        return $this->put_facilitator( $notification );
    }

    /**
     * Delete facilitator via webhook from SeminarDesk
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function delete_facilitator($notification)
    {
        $payload = (array)$notification['payload'];

        /**
         * sd_cpt_facilitator
         */
        $facilitator_deleted = Utils::delete_post_by_meta('sd_cpt_facilitator', 'sd_facilitator_id', $payload['id']);
        if ( empty($facilitator_deleted) ){
            return array(
                'status'        => 404,
                'message'       => 'Nothing to delete. Facilitator ID ' . $payload['id'] . ' does not exists',
                'action'        => $notification['action'],
                'eventDateId'   => $payload['id'],
            );
        }

        /**
         * sd_txn_facilitators
         */
        $term = get_term_by( 'name', $payload['id'], 'sd_txn_facilitators', ARRAY_A );
        $term_deleted = wp_delete_term( $term['term_id'], 'sd_txn_facilitators' );

        if ( $term_deleted === false ){
            return array(
                'status'        => 404,
                'message'       => 'Cannot delete term. Facilitator ID ' . $payload['id'] . ' does not exists',
                'action'        => $notification['action'],
                'facilitatorId'   => $payload['id'],
            );
        } elseif ( $term_deleted === 0 ){
            return array(
                'status'        => 404,
                'message'       => 'Attempt to delete default term',
                'action'        => $notification['action'],
                'facilitatorId'   => $payload['id'],
            );
        } elseif ( ( is_wp_error( $term_deleted ) ) ){
            return array(
                'status'        => 404,
                'message'       => 'Not deleted. Term corresponding with Facilitator ID ' . $payload['id'] . ' does not exists',
                'action'        => $notification['action'],
                'facilitatorId'   => $payload['id'],
            );
        }
        
        return array(
            'status'            => 200,
            'message'           => 'Facilitator deleted',
            'action'            => $notification['action'],
            'facilitatorId'     => $payload['id'],
            'facilitatorWpId'   => $facilitator_deleted->ID,
        );
    }

    /**
     * Create or update label group and its labels via webhook from SeminarDesk
     *
     * @param array $notification 
     * @return WP_REST_Response|WP_Error
     */
    public function put_label( $notification )
    {        
        $payload = (array)$notification['payload']; // payload of the request in JSON
        $sd_webhook = $notification;
        unset($sd_webhook['payload']);

        /**
         * sd_txn_labels
         */
        // labelGroup term and its meta
        $lg_term_name = 'lg_id_' . $payload['id'];
        $lg_term_ids = Utils::set_term( 'sd_txn_labels', $lg_term_name, $payload['name'], $payload['name'] ); // Note: set ID as name to be queryable
        // return error if $term_ids is of type WP_Error
        if (is_wp_error($lg_term_ids)){
            return $lg_term_ids;
        }
        $lg_term_id = $lg_term_ids['term_id'];
        $lg_term_meta_id = Utils::set_term_meta( $lg_term_ids, 'sd_data', $payload );
        if (is_wp_error($lg_term_meta_id)){
            return $lg_term_meta_id;
        }
        
        /**
         * sd_cpt_label
         */        
        // set labelGroup post
        $lg_query = Utils::get_query_by_meta( 'sd_cpt_label', 'sd_labelGroup_id', $payload['id']);
        $lg_post_id = $lg_query->post->ID ?? 0;
        $lg_count = $lg_query->post_count;
        // define metadata of the event
        $lg_meta_input = array(
            'sd_labelGroup_id'  => $payload['id'],
            'sd_data'           => $payload,
            'sd_webhook'        => $sd_webhook,
        );
        // set attributes of the new event
        $lg_attr = array(
            'post_type'     => 'sd_cpt_label',
            'post_title'    => $payload['name'],
            'post_name'     => $payload['name'],
            'post_author'   => get_current_user_id(),
            'post_status'   => 'publish',
            'meta_input'    => $lg_meta_input,
            // 'tax_input'     => $txn_input,
        );
        // create new post or update post with existing post id
        if ( !empty($lg_post_id) ) {
            $lg_attr['ID'] = $lg_post_id;
            $message = 'Label Group updated';
            $lg_post_id = wp_update_post( wp_slash($lg_attr), true );
        } else {
            $message = 'Label Group created';
            $lg_post_id = wp_insert_post( wp_slash($lg_attr), true );
        }

        // return error if $post_id is of type WP_Error
        if (is_wp_error($lg_post_id)){
            return $lg_post_id;
        }

        $txn_input = array();
        $labelGroup = $payload ?? null;
        if ( !empty( $labelGroup ) ){
            Utils::add_term_tax_input( $txn_input, $lg_term_name, 'sd_txn_labels' );
        }
        
        // set label posts and term object
        $labels_payload = $payload['labels'];

        // delete label terms and posts if not included in the labelGroup anymore
        $label_posts = get_posts( array(
            'post_type'     => 'sd_cpt_label',
            'post_parent'   => $lg_post_id,
        ) );
        foreach ( $label_posts as $label_post ){
            Utils::exclude_label_post( $label_post, $labels_payload );
        }
        $label_terms = get_terms( array(
            'taxonomy'  => 'sd_txn_labels',
            'parent'    => $lg_term_id,
            'hide_empty' => false,
        ) );
        foreach ( $label_terms as $label_term ){
            Utils::exclude_label_term( $label_term, $labels_payload );
        }
        
        foreach ( $labels_payload as $label_payload ) {
            $label_query = Utils::get_query_by_meta( 'sd_cpt_label', 'sd_label_id', $label_payload['id']);
            $label_post_id = $label_query->post->ID ?? 0;
            $label_count = $label_query->post_count;
            // define metadata of the event
            $label_meta_input = array(
                'sd_label_id'   => $label_payload['id'],
                'sd_data'       => $label_payload,
                'sd_webhook'    => $sd_webhook,
            );
            // set attributes of the new event
            $label_slug = Utils::unique_post_slug( $label_payload['name'], $label_post_id );
            $label_attr = array(
            'post_type'     => 'sd_cpt_label',
            'post_title'    => $label_payload['name'],
            'post_name'     => $label_slug,
            'post_author'   => get_current_user_id(),
            'post_status'   => 'publish',
            'post_parent'   => $lg_post_id,
            'meta_input'    => $label_meta_input,
            'tax_input'     => $txn_input,
            );
            // create or update post with existing post id
            if ( !empty($label_post_id) ) {
                $label_attr['ID'] = $label_post_id;
                $label_post_id = wp_update_post( wp_slash($label_attr), true );
            } else {
                $label_post_id = wp_insert_post( wp_slash($label_attr), true );
            }
            // return error if $post_id is of type WP_Error
            if (is_wp_error($label_post_id)){
                return $label_post_id;
            }
            // label terms and their meta 
            $label_term_name = 'l_id_' .  $label_payload['id'];
            $label_term_description = $label_payload['name'];
            // Note: labels have duplicated 'name', results in slug of 'name' . '_' . 'parent slug' (e.g. label01 -> label01-label-group-02). This is default behaviour of WordPress for terms
            $label_ids = Utils::set_term( 'sd_txn_labels', $label_term_name, $label_term_description, $label_slug, $lg_term_id );
            if (is_wp_error($label_ids)){
                return $label_ids;
            }
            $label_meta_id = Utils::set_term_meta( $label_ids, 'sd_data', $label_payload );
            if (is_wp_error($label_meta_id)){
                return $label_meta_id;
            }
        }

        return array(
            'status'            => 200,
            'message'           => $message,
            'action'            => $notification['action'],
            'labelGroupId'      => $payload['id'],
            'labelGroupWpId'    => $lg_post_id,
            'labelCount'        => $label_count,
        );
    }

     /**
     * Create label group and its labels via webhook from SeminarDesk
     * Incase facilitator id already exists, it will update existing facilitator
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function create_label($notification)
    {
        return $this->put_label( $notification );
    }

    /**
     * Update label group and its labels via webhook from SeminarDesk
     * Incase id doesn't exists, it will create the label group/label
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function update_label($notification)
    {
        return $this->put_label( $notification );
    }

    /**
     * Delete label group and its labels via webhook from SeminarDesk
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function delete_label($notification)
    {
        $payload = (array)$notification['payload'];
        $lg_query = Utils::get_query_by_meta('sd_cpt_label', 'sd_labelGroup_id', $payload['id'] );
        $lg_post_id = $lg_query->post->ID;

        /**
         * sd_cpt_label
         */
        // delete all labels of the labelGroup
        $label_posts = get_posts( array(
            'post_type' => 'sd_cpt_label',
            'post_parent'   => $lg_post_id,
        ) );
        foreach( $label_posts as $label_post ){
            $label_deleted = wp_delete_post( $label_post->ID );
            $label_wp_ids[] = $label_deleted->ID;
        }
        // delete labelGroup
        $lg_deleted = wp_delete_post( $lg_post_id );
        if ( empty($lg_deleted) ){
            return array(
                'status'        => 404,
                'message'       => 'Nothing to delete. LabelGroup ID ' . $payload['id'] . ' does not exists',
                'action'        => $notification['action'],
                'labelGroupId'   => $payload['id'],
            );
        }

        /**
         * sd_txn_labels
         */
        // delete all labels of the labelGroup
        $lg_term_name = 'lg_id_' . $payload['id'];
        $lg_term = get_term_by( 'name', $lg_term_name, 'sd_txn_labels' );
        $label_terms = get_terms( array(
            'taxonomy'  => 'sd_txn_labels',
            'parent'    => $lg_term->term_id,
            'hide_empty' => false,

        ) );
        // delete labelGroup
        $term_deleted = wp_delete_term( $lg_term->term_id, 'sd_txn_labels' );
        if ( $term_deleted === false ){
            return array(
                'status'        => 404,
                'message'       => 'Cannot delete term. Label Group ID ' . $payload['id'] . ' does not exists',
                'action'        => $notification['action'],
                'labelGroupId'   => $payload['id'],
            );
        } elseif ( $term_deleted === 0 ){
            return array(
                'status'        => 404,
                'message'       => 'Attempt to delete default term',
                'action'        => $notification['action'],
                'labelGroupId'   => $payload['id'],
            );
        } elseif ( ( is_wp_error( $term_deleted ) ) ){
            return array(
                'status'        => 404,
                'message'       => 'Cannot delete term. Label Group ID ' . $payload['id'] . ' does not exists',
                'action'        => $notification['action'],
                'labelGroupId'   => $payload['id'],
            );
        }
        // delete all labels of the labelGroup
        foreach( $label_terms as $label_term ){
            $term_sd_data = get_term_meta( $label_term->term_id, 'sd_data', true );
            $term_deleted = wp_delete_term( $label_term->term_id, 'sd_txn_labels' );
            if ( $term_deleted === false ){
                return array(
                    'status'        => 404,
                    'message'       => 'Cannot delete term. Label ID ' . $term_sd_data['id'] . ' does not exists',
                    'action'        => $notification['action'],
                    'labelId'   => $term_sd_data['id'],
                );
            } elseif ( $term_deleted === 0 ){
                return array(
                    'status'        => 404,
                    'message'       => 'Attempt to delete default term',
                    'action'        => $notification['action'],
                    'labelId'   => $term_sd_data['id'],
                );
            } elseif ( ( is_wp_error( $term_deleted ) ) ){
                return array(
                    'status'        => 404,
                    'message'       => 'Cannot delete term. Label Group ID ' . $term_sd_data['id'] . ' does not exists',
                    'action'        => $notification['action'],
                    'labelId'   => $term_sd_data['id'],
                );
            }
    
            $label_ids[] = $term_sd_data['id'];
        }


        return array(
            'status'            => 200,
            'message'           => 'Label Group deleted',
            'action'            => $notification['action'],
            'groupLabelId'      => $payload['id'],
            'groupLabelWpId'    => $lg_deleted->ID,
            'labelIds'          => $label_ids,
            'labelWpIds'        => $label_wp_ids,
        );
    }

    /**
     * delete all SeminarDesk cpts, txns and their terms via webhook from database - for debugging
     *
     * @param array $notification
     * @return WP_REST_Response|WP_Error
     */
    public function delete_all($notification)
    {
        $delete_all = AdminUtils::get_option_or_default(SD_OPTION['delete'], false);
        $debug =  AdminUtils::get_option_or_default( SD_OPTION['debug'], false);
        if ( $delete_all !== false && $debug !== false ) {
            // Get SeminarDesk's posts and terms and delete them
            AdminUtils::delete_all_sd_objects();
            return array(
                'status'            => 200,
                'message'           => 'Plugin cleared - all cpt, txn, term deleted from the data base',
                'action'            => $notification['action'],
            );
        }
        return array(
            'status'        => 405,
            'message'       => 'Plugin not cleared - Not Allowed. Debug and Deleted All needs to be enabled at the admin page of the SD plugin',
            'action'        => $notification['action'],
        );
    }
}