<?php

/**
 * The template for taxonomy sd_txn_dates with past event dates
 * 
 * @package SeminardeskPlugin
 */

namespace Inc\Controllers;

use Inc\Utils\AdminUtils;
use WP_Query;

// Note: Show/Edit Taxonomy http://localhost/wpsdp/wp-admin/edit-tags.php?taxonomy=sd_txn_dates
class QueryController
{
    /**
     * Register taxonomies via controller class
     *
     * @return void
     */
    public function register()
    {
        add_action( 'pre_get_posts', array( $this, 'modify_query' ) );
        add_filter( 'template_include', array( $this, 'overwrite_query' ), 20 );  
    }
    
    /**
     * modify the query of taxonomy
     * 
     * @param WP_Query $query 
     * @return void 
     */
    public function modify_query( $query ) {

        /**
         * sd_txn_dates
         */
        if ( $query->is_tax() && array_key_exists( 'sd_txn_dates', $query->query) && $query->is_main_query() ) {
            $query->set( 'meta_key', 'sd_date_begin' );
            $query->set( 'orderby', 'meta_value_num' );
            $query->set( 'order', 'ASC' );
            $query->set( 'meta_query', array(
                'key'       => 'preview_available',
                'value'     => true,
                ) );
            // $query->set( 'posts_per_page', '5' ); // debugging
        }

        /**
         * sd_txn_labels
         */
        if ( $query->is_tax() && array_key_exists( 'sd_txn_labels', $query->query) && $query->is_main_query() ) {
            $query->set( 'post_status', 'any');
            $query->set( 'orderby', 'title' );
            $query->set( 'order', 'ASC' );
            // rewrite tax_query to exclude children
            $tax_query = $query->tax_query->queries[0];
            $tax_query['include_children'] = false;
            $query->tax_query->queries[0] = $tax_query;
            $query->query_vars['tax_query'] = $query->tax_query->queries;
        }

        /**
         * sd_archive
         */
        if ( $query->is_archive() && $query->is_tax() === false && $query->is_main_query() ){
            $post_type = $query->query['post_type'] ?? null;
            $query->set( 'post_status', 'any');
            if ( $post_type === 'sd_cpt_facilitator' ){
                $query->set( 'meta_key', 'sd_last_name' );
                $query->set( 'orderby', array( 
                    'meta_value'    => 'ASC',
                    'title'         => 'ASC', 
                ) );
            } else {
                $query->set( 'orderby', 'title' );
                $query->set( 'order', 'ASC' );
            }
            if ( $post_type === 'sd_cpt_label' ){
                $query->set( 'post_parent', 0 );
            }
        }
    }


    /**
     * overwrite the current query by a custom query 
     * 
     * @param string $template template path
     * @return string template path
     */
    public function overwrite_query( string $template )
    {
        global $wp_query;
        if ( is_tax() && !empty( get_query_var('sd_txn_dates') ) && is_main_query() ) {
            $queried_object = get_queried_object();
            switch ( $queried_object->name ){
                case 'upcoming':
                    $timestamp_today = strtotime(wp_date('Y-m-d')); // current time
                    // $timestamp_today = strtotime('2020-01-01'); // debugging
                    $paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
                    $wp_query = new WP_Query( array(
                        'post_type'        => 'sd_cpt_date',
                        'post_status'      => 'publish',
                        // 'posts_per_page'   => '5', // debugging
                        'paged'            => $paged,
                        'meta_key'         => 'sd_date_begin',
                        'orderby'          => 'meta_value_num',
                        'order'            => 'ASC',
                        'term_object'      => $queried_object, // custom var
                        'meta_query'       => array(
                            'relation'  => 'AND',
                            array(
                                'key'       => 'sd_date_begin',
                                'value'     => $timestamp_today*1000, //in ms
                                'type'      => 'numeric',
                                'compare'   => '>=',
                            ),
                            array(
                                'key'       => 'preview_available',
                                'value'     => true,
                            ),
                        ),
                    ) );
                    // when loop is finished
                    add_action( 'get_footer', array( $this, 'reset_query' ), 100 );
                    break;
                case 'past':
                    $timestamp_today = strtotime(wp_date('Y-m-d')); // current time
                    // $timestamp_today = strtotime('2020-08-01'); // debugging
                    $paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
                    $wp_query = new WP_Query( array(
                        'post_type'        => 'sd_cpt_date',
                        'post_status'      => 'publish',
                        // 'posts_per_page'   => '5', // debugging
                        'paged'            => $paged,
                        'meta_key'         => 'sd_date_begin',
                        'orderby'          => 'meta_value_num',
                        'order'            => 'DESC',
                        'term_object'      => $queried_object, // custom var
                        'meta_query'       => array(
                            'relation'  => 'AND',
                            array(
                                'key'       => 'sd_date_begin',
                                'value'     => $timestamp_today*1000, //in ms
                                'type'      => 'numeric',
                                'compare'   => '<',
                            ),
                            array(
                                'key'       => 'preview_available',
                                'value'     => true,
                            ),
                        ),
                    ) );
                    // when loop is finished
                    add_action( 'get_footer', array( $this, 'reset_query' ), 100 );
                break;
                default:
                break;
            }
        }
        return $template;
    }

    /**
     * Reset to the initial WP query stored in $wp_the_query
     * 
     * Note : the initial query is stored in another global named $wp_the_query
     * 
     * @return void 
     */
    public function reset_query()
    {
        global $wp_query, $wp_the_query;
        switch ( current_filter() ) {
            case 'get_footer':
                $wp_query = $wp_the_query;
            break;
        }
    }
}