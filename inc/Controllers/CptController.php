<?php

/**
 * @package SeminardeskPlugin
 */

namespace Inc\Controllers;

use Inc\Utils\AdminUtils;

/**
 * Handles CPTs during init and activation
 */
class CptController
{
    /**
     * Register cpts via controller class
     *
     * @return void
     */
    public function register()
    {
        add_action( 'init', array( $this, 'create_cpts' ) );
    }

    /**
     * create custom post types for SeminarDesk
     * 
     * @return void 
     */
    public function create_cpts( )
    {
        foreach (SD_CPT as $key => $value){
            $name = ucfirst($value['name']);
            $names = ucfirst($value['names']);
            $title = ucfirst($value['title']);
            $name_lower = strtolower($value['name']);
            $names_lower = strtolower($value['names']);
            $show_ui = AdminUtils::get_option_or_default( SD_OPTION['debug'], false );
            $slug = AdminUtils::get_option_or_default( SD_OPTION['slugs'] , $value['slug_default'], $value['slug_option_key'] );

            /**
             * array to configure labels for the CPT 
             */
            $labels = array(
                'name'                  => $names,
                'singular_name'         => $name,
                'name_admin_bar'        => $name,
                'add_new'               => __( 'Add New', 'seminardesk' ),
                'add_new_item'          => __( 'Add New', 'seminardesk' ) . ' ' . $name,
                'new_item'              => __( 'New', 'seminardesk' ) . ' ' . $name,
                'edit_item'             => __( 'Edit', 'seminardesk' ) . ' ' . $name,
                'view_item'             => __( 'View', 'seminardesk' ) . ' ' . $name,
                'view_items'            => __( 'View', 'seminardesk' ) . ' ' . $names,
                // 'all_items'          => __( 'All', 'seminardesk' ) . ' ' . $names,
                'all_items'             => $title,
                'search_items'          => __( 'Search', 'seminardesk' )  . ' ' . $names,
                'parent_item_colon'     => __( 'Parent', 'seminardesk' ) . ' ' . $names . ':',
                'not_found'             => __( 'No found' , 'seminardesk' ) . ' ' . $names_lower,
                'not_found_in_trash'    => __( 'No found in Trash', 'seminardesk' ) . ' ' . $names_lower,
                'parent_item_colon'     => __( 'Parent', 'seminardesk' ) . ' ' . $name,
                'archives'              => $name . ' ' . __( 'Archives', 'seminardesk' ),
                'attributes'            => $name . ' ' . __( 'Attributes', 'seminardesk' ),
                'insert_into_item'      => __( 'Insert into', 'seminardesk' ) . ' ' . $name_lower,
                'uploaded_to_this_item' => __( 'Uploaded to this', 'seminardesk' ) . ' ' . $name_lower,
            );

            /**
             * array to set rewrite rules for the CPT (sub CPT option)
             */
            $rewrite = array(
                'slug' => $slug,
            );

            /**
             * array to registers supported features for the CPT (sub CPT option)
             */
            $supports = array(
                'title', 
                'editor', 
                'author', 
                //'thumbnail', 
                'excerpt', 
                'custom-fields', // enable support of Meta API
                'page-attributes', // template and parent, hierarchical must be true for parent option
                //'post-formats',
            );

            /**
             * array to configure CPT options
             */
            $cptOptions =  array(
                'labels'                => $labels,
                'description'           => $name . ' ' . __( 'post type for SeminarDesk.', 'seminardesk' ),
                'has_archive'           => $value['has_archive'],
                'show_in_rest'          => false,
                'public'                => $value['public'],
                'exclude_from_search'   => $value['exclude_from_search'],
                'show_ui'               => $show_ui,
                'show_in_menu'          => SD_ADMIN['page'], // add post type to the seminardesk menu
                'menu_position'         => $value['menu_position'],
                'supports'              => $supports,
                'rewrite'               => $rewrite,
                'taxonomies'            => $value['taxonomies'],
                'hierarchical'          => $value['hierarchical'],
            );

            register_post_type( $key, $cptOptions ); 

            // for debugging custom post type features... expensive operation. should usually only be called when activate and deactivate the plugin
            // flush_rewrite_rules();
        } 

    }
}