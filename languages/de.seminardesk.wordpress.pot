# Copyright (C) 2020 SeminarDesk – Danker, Smaluhn & Tammen GbR
# This file is distributed under the GPLv2 or later.
msgid ""
msgstr ""
"Project-Id-Version: SeminarDesk for WordPress 1.2.0-SNAPSHOT\n"
"Report-Msgid-Bugs-To: "
"https://wordpress.org/support/plugin/seminardesk-wordpress\n"
"POT-Creation-Date: 2020-11-19 17:20:56+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2020-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n 1.0.3\n"

#: admin/admin.php:27
msgid "Save Settings"
msgstr ""

#: inc/Callbacks/ManagerCallbacks.php:108
msgid ""
"Customize the slugs of this plugin. Use with caution, might have unintended "
"effects on a deployed WordPress instance."
msgstr ""

#: inc/Callbacks/ManagerCallbacks.php:113
msgid "Manage the settings for development."
msgstr ""

#: inc/Callbacks/ManagerCallbacks.php:119
msgid "Manage the settings for WP uninstaller of the SeminarDesk plugin."
msgstr ""

#: inc/Controllers/AdminController.php:180
msgid "Slugs"
msgstr ""

#: inc/Controllers/AdminController.php:186
msgid "Developing"
msgstr ""

#: inc/Controllers/AdminController.php:192
msgid "Uninstall"
msgstr ""

#: inc/Controllers/AdminController.php:206
msgid "Debug:"
msgstr ""

#: inc/Controllers/AdminController.php:217
msgid "Delete all:"
msgstr ""

#: inc/Controllers/CptController.php:49 inc/Controllers/CptController.php:50
#: inc/Controllers/TaxonomyController.php:52
msgid "Add New"
msgstr ""

#: inc/Controllers/CptController.php:51
msgid "New"
msgstr ""

#: inc/Controllers/CptController.php:52
#: inc/Controllers/TaxonomyController.php:50
msgid "Edit"
msgstr ""

#: inc/Controllers/CptController.php:53 inc/Controllers/CptController.php:54
msgid "View"
msgstr ""

#: inc/Controllers/CptController.php:57
#: inc/Controllers/TaxonomyController.php:46
msgid "Search"
msgstr ""

#: inc/Controllers/CptController.php:58 inc/Controllers/CptController.php:61
#: inc/Controllers/TaxonomyController.php:48
#: inc/Controllers/TaxonomyController.php:49
msgid "Parent"
msgstr ""

#: inc/Controllers/CptController.php:59
msgid "No found"
msgstr ""

#: inc/Controllers/CptController.php:60
msgid "No found in Trash"
msgstr ""

#: inc/Controllers/CptController.php:62
msgid "Archives"
msgstr ""

#: inc/Controllers/CptController.php:63
msgid "Attributes"
msgstr ""

#: inc/Controllers/CptController.php:64
msgid "Insert into"
msgstr ""

#: inc/Controllers/CptController.php:65
msgid "Uploaded to this"
msgstr ""

#: inc/Controllers/CptController.php:94
msgid "post type for SeminarDesk."
msgstr ""

#: inc/Controllers/TaxonomyController.php:47
msgid "All"
msgstr ""

#: inc/Controllers/TaxonomyController.php:51
msgid "Update"
msgstr ""

#: inc/Controllers/TaxonomyController.php:53
msgid "New Name"
msgstr ""

#: inc/Utils/WebhookHandler.php:317 inc/Utils/WebhookHandler.php:324
msgid "Dates of"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "SeminarDesk for WordPress"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://www.seminardesk.com/wordpress-plugin"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Allows you to connect SeminarDesk to your WordPress site in order to create "
"posts for events, dates and facilitators."
msgstr ""

#. Author of the plugin/theme
msgid "SeminarDesk – Danker, Smaluhn & Tammen GbR"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://www.seminardesk.com/"
msgstr ""

#: seminardesk-wordpress.php:99
msgctxt "post type singular name"
msgid "Event"
msgstr ""

#: seminardesk-wordpress.php:115
msgctxt "post type singular name"
msgid "Date"
msgstr ""

#: seminardesk-wordpress.php:131
msgctxt "post type singular name"
msgid "Facilitator"
msgstr ""

#: seminardesk-wordpress.php:146
msgctxt "post type singular name"
msgid "Label"
msgstr ""

#: seminardesk-wordpress.php:100
msgctxt "post type general name"
msgid "Events"
msgstr ""

#: seminardesk-wordpress.php:116
msgctxt "post type general name"
msgid "Dates"
msgstr ""

#: seminardesk-wordpress.php:132
msgctxt "post type general name"
msgid "Facilitators"
msgstr ""

#: seminardesk-wordpress.php:147
msgctxt "post type general name"
msgid "Labels"
msgstr ""

#: seminardesk-wordpress.php:101
msgctxt "post type setting title"
msgid "CPT Events"
msgstr ""

#: seminardesk-wordpress.php:117
msgctxt "post type setting title"
msgid "CPT Dates"
msgstr ""

#: seminardesk-wordpress.php:133
msgctxt "post type setting title"
msgid "CPT Facilitators"
msgstr ""

#: seminardesk-wordpress.php:148
msgctxt "post type setting title"
msgid "CPT Labels"
msgstr ""

#: seminardesk-wordpress.php:169
msgctxt "taxonomy singular name"
msgid "Date"
msgstr ""

#: seminardesk-wordpress.php:183
msgctxt "taxonomy singular name"
msgid "Facilitator"
msgstr ""

#: seminardesk-wordpress.php:197
msgctxt "taxonomy singular name"
msgid "Label"
msgstr ""

#: seminardesk-wordpress.php:170
msgctxt "taxonomy general name"
msgid "Dates"
msgstr ""

#: seminardesk-wordpress.php:184
msgctxt "taxonomy general name"
msgid "Facilitators"
msgstr ""

#: seminardesk-wordpress.php:198
msgctxt "taxonomy general name"
msgid "Labels"
msgstr ""

#: seminardesk-wordpress.php:171
msgctxt "taxonomy setting title"
msgid "TXN Dates"
msgstr ""

#: seminardesk-wordpress.php:185
msgctxt "taxonomy setting title"
msgid "TXN Facilitator"
msgstr ""

#: seminardesk-wordpress.php:199
msgctxt "taxonomy setting title"
msgid "TXN Labels"
msgstr ""

#: seminardesk-wordpress.php:219
msgctxt "term title"
msgid "Upcoming Events Dates"
msgstr ""

#: seminardesk-wordpress.php:226
msgctxt "term title"
msgid "Past Events Dates"
msgstr ""