=== SeminarDesk for WordPress ===
Requires at least: 5.2
Tested up to: 5.5.3
Requires PHP: 7.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Connects SeminarDesk to WordPress.

== Description ==

This plugin allows you to connect [SeminarDesk](https://www.seminardesk.com) to your WordPress site in order to automatically create posts for events, dates and facilitators when those items are created or updated via SeminarDesk.

== Installation ==

The plugin can be installed via ZIP file, the most recent version is provided [here](https://www.seminardesk.com/wordpress-plugin).

If you have direct access to your hosting environment, you can also clone the [plugin's Git repository](https://bitbucket.org/seminardesk/seminardesk-wordpress/branch/master) and checkout the master branch.

== Setup ==

The plugin works by handling the Webhooks that are triggered by SeminarDesk.

You need to complete the following steps in order to create the connection:

At first, create a new WordPress user with "Author" role, you could name it `SeminarDesk`, for example.

Then add the following URL in SeminarDesk under "Administration > Webhooks", alongside the username and password of the user you created in step 1.

`https://**your-wordpress-site.com**/wp-json/seminardesk/v1/webhooks`

When adding the webhook URL, you should select "All events" so that all supported item types will be published to your WordPress site.

From now on, SeminarDesk will publish events to the WordPress plugin whenever a new item like event or facilitator is created, updated or deleted. To initially publish all items, you can select "Send all items" from the webhook's action menu.

== Customization ==

= Slugs =

In WordPress the events and facilitators can be accessed through URL addresses, which are defined by slugs. You can customize the slugs in the WordPress Dashboard "SeminarDesk > Settings" to generate your own custom URL addresses. Remember that the slugs need to be unique in WordPress.

List of available slugs and their default url address:

    - events: `./**your-wordpress-root**/events`
    - dates: `./**your-wordpress-root**/schedule`
        - upcoming dates: `./**your-wordpress-root**/schedule/upcoming`
        - past dates: `./**your-wordpress-root**/schedule/past`
        - year: `./**your-wordpress-root**/schedule/year` (not customizable)
        - year-month: `./**your-wordpress-root**/schedule/year-month` (not customizable)
    - facilitators: `./**your-wordpress-root**/facilitators`

= Templates =

The SeminarDesk Plugin comes with a number of template files, that determine how the plugin looks and behaves. You can create your own templates by placing copies of them in a separate folder and customizing this copies to your needs. It’s important that you don’t edit the template files directly in the plugin. This ensures that any changes you make to the template files will not be lost, when the plugin is updated to a new version. Here are the steps you can follow to create your custom templates (e.g. via ftp):

1. Locate the default template files: `./**your-wordpress-root**/wp-content/plugin/seminardesk-wordpress/templates`
2. Create the separate template folder
    Option 1: in the wordpress plugin folder: `./**your-wordpress-root**/wp-content/plugin/seminardesk-custom/templates`
    Option 2: in your theme folder `./**your-wordpress-root**/wp-content/theme/**your-theme**/seminardesk-custom/templates`
3. Copy the default template files in the created folder
4. Customize the copied template files to your needs...
    Optional: Create asset files with the same name than the template (e.g. sd_cpt_event.{css, js}) in the subfolder 'assets' of your custom template folder

== Changelog ==

= 1.0.1 =
* Renamed plugin file, added readme file and added Autoload generated sources and dependencies to make plugin installable via Git.

= 1.0.0 =
* Initial release.
